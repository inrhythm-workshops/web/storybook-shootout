const cssModulesPlugin = require("esbuild-css-modules-plugin");

require('esbuild').build({
    entryPoints: ['src/index.jsx'],
    bundle: true,
    minify: true,
    sourcemap: true,
    target: ['chrome58', 'firefox57', 'safari11', 'edge16'],
    outfile: './public/dist/bundle.js',
    plugins: [cssModulesPlugin()],
  })