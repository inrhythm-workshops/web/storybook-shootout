import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import html from '@rollup/plugin-html';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import livereload from "rollup-plugin-livereload";
import postcss from 'rollup-plugin-postcss';
import serve from "rollup-plugin-serve";
import { terser } from "rollup-plugin-terser";

export default {
    input: "src/index.js",
    output: [
        {
            file: "./dist/bundle.js",
            format: "iife",
            sourcemap:false,
        }
    ],
  plugins: [
    postcss({
        extensions: [".css"],
      }),
      html(),
    nodeResolve({
      extensions: [".js"],
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify( 'development' )
    }),
    babel({
      presets: ["@babel/preset-react"],
    }),
    commonjs(),
    process.env.NODE_ENV !== 'production' && serve({
      open: true,
      verbose: true,
      contentBase: ["", "public"],
      host: "localhost",
      port: 3000,
    }),
    process.env.NODE_ENV !== 'production' && livereload({ watch: "dist" }),
    process.env.NODE_ENV === 'production' && terser(),
  ]
};