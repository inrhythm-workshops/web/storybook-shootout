import { Movie } from '../interfaces'

type MovieProps = {
  movie: Movie
}

export default function MovieComponent({ movie }: MovieProps) {
  /**
  * Return a list item containing a Link to a movie, similar to PersonComponent in components/Person.tsx
  */
}