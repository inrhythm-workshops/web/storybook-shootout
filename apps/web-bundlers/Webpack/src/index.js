import { createElement } from "react";
import * as ReactDOM from "react-dom";
import { App } from "./App";
import "./App.css";

ReactDOM.render(createElement(App), document.getElementById("root"));