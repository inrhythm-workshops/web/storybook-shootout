const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const path = require( 'path' );

const webpackPlugins = [
  new HtmlWebpackPlugin( {
    template: path.resolve( __dirname, 'src/index.html' ),
    filename: 'index.html',
  } ),
];

module.exports = {
  context: __dirname,
  entry: './src/index.js',
  output: {
    path: path.resolve( __dirname, 'dist' ),
    filename: 'main.js',
    publicPath: '/',
  },
  devServer: {
    port: 3001,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.css?$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.(png|j?g|svg|gif)?$/,
        use: 'file-loader?name=./images/[name].[ext]',
      },
    ],
  },
  plugins: webpackPlugins,
};