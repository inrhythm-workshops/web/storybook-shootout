# storybook-shootout

### Setup

This project uses Node 16.19.1 as storybook doesn't work well with Node 18 as of yet.

To setup this exercise, open two Terminal windows side by side 
In the first Terminal window run the following command:

```
cd storybook-shootout
npm i
cd apps/storybook-webpack
npm run storybook
```

In the second Terminal window run the following command:
```
cd storybook-shootout/apps/storybook-vite
npm run storybook
```

Or run all the apps together with the following command:
```
cd storybook-shootout
npm i
npm run dev
```

### Build

To build all apps and packages, run the following command:

```
cd storybook-shootout
npm run build
```